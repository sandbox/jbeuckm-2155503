<?php

/**
 * Implements hook_views_data_alter()
 * @param $data
 */
function unpublish_button_views_data_alter(&$data) {

    $data['node']['unpublish'] = array(
        'title' => t('Unpublish'),
        'help' => t('Button to quickly unpublish a node.'),
        'field' => array(
            'handler' => 'unpublish_button_views_handler_field_unpublish',
            'group' => 'Content',
            'click sortable' => FALSE,
        ),
    );
}