<?php
/**
 * @file
 * handler class for the new field 'unpublish'
 */
/**
 * class definition
 */
class unpublish_button_views_handler_field_unpublish extends views_handler_field {
    /**
     * Render function: return html output
     * Including unpublish button
     */
    function render($values) {

        $button = array(

            '#type' => 'form',
            '#action' => 'node/unpublish/'.$values->nid,
            'button' => array(
                '#type' => 'button',
                '#value' => t('Unpublish')
            )

        );

        return render($button);
    }

    /**
     *
     */
    function query() {
        // Do nothing, leave query blank, we render the contents
    }
}

