<?php


function _unpublish_button_form($form, &$form_state, $node_id) {

    global $user;

    $node = node_load($node_id);
    if ($node->uid != $user->uid) {
        drupal_access_denied();
        module_invoke_all('exit');
        exit();
    }

    $emw = entity_metadata_wrapper('node', $node);

    return array(
        'node_id' => array(
            '#type' => 'hidden',
            '#value' => $node_id
        ),
        'node' => array(
            '#type' => 'markup',
            '#markup' => $emw->title->value(array('sanitize' => TRUE)),
        ),
        'actions' => array(
            '#type' => 'actions',
            'approve' => array(
                '#type' => 'submit',
                '#value' => t('Unpublish'),
            ),
            'cancel' => array(
                '#type' => 'submit',
                '#value' => t('Cancel'),
            )
        )
    );
}

function _unpublish_button_form_submit($form, &$form_state) {

    switch ($form_state['triggering_element']['#value']) {
        case 'Unpublish':

            $node_id = $form_state['values']['node_id'];
            $node = node_load($node_id);
            $emw = entity_metadata_wrapper('node', $node);

            $emw->status->set(0);
            $emw->save();

            break;
        case 'Cancel':
            break;
    }

    drupal_goto();
}


